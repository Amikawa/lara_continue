<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ваш пароль был удалён!',
    'sent' => 'Вы вам отправили письмо со ссылкой удалёния пароля!',
    'throttled' => 'Пожалуйста, подождите перед повторной попыткой.',
    'token' => 'Данный токен не валиден.',
    'user' => "Не можем найти пользователя с данной почтой.",
    'Password' => 'пароль',

];
