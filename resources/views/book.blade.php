@extends('layouts.master')

@section('title', 'Книга')

@section('content')
    <h1>{{$book->name}}</h1>
    <img src="{{ Storage::url($book->image) }}">
    <p>Количество: <b>{{$book->total}}.</b></p>

    <p>{{$book->description}}</p>

    <form action="{{route('basket-add', $book)}}" method="POST">
        @csrf
        @if($book->isAvailable())
            <button type="submit">Добавить в корзину</button>
        @else
            Не доступен
        @endif
    </form>
    @comments([
    'model' => $book,
    'approved' => true
    ])
@endsection
