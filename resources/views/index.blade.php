@extends('layouts.master')

@section('title', 'Главная')

@section('content')
    <h1>Все книги</h1>
    <form>
        <div class="row form-group" style="display: flex;flex-direction: row; justify-content: space-between">
            <div class="input-group">
                {{--                    <div class="alert alert-warning">{{ $message }}</div>--}}
                <label for="category" class="col-form-label">Категория</label>
                <input type="text" class="form-control-input" name="category" id="category"
                       value="{{ request()->category}}">
            </div>

            <div class="input-group">
                {{--                    <div class="alert alert-warning">{{ $message }}</div>--}}
                <label for="book" class="col-form-label">Книга: </label>
                <input type="text" class="form-control-input" name="book" id="book"
                       value="{{ request()->book}}">
            </div>

            <div class="input-group">
                {{--                    <div class="alert alert-warning">{{ $message }}</div>--}}
                <label for="author" class="col-form-label">Автор: </label>
                <input type="text" class="form-control-input" name="author" id="author"
                       value="{{ request()->author}}">
            </div>

            <div class="input-group">
                <button type="submit" class="btn btn-primary">Фильтр</button>
                <a href="{{ route("index") }}" class="btn btn-warning">Сброс</a>
            </div>
        </div>
    </form>
    <div class="row">
        @foreach($books as $book)
            @include('layouts.card', compact('book'))
        @endforeach
    </div>
    {{$books->withQueryString()->links()}}
@endsection
