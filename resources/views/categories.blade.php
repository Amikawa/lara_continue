@extends('layouts.master')

@section('title', 'Все категории')

@section('content')

    <div id="v-categories">
        <v-categories/>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>

@endsection
