@extends('layouts.master')

@section('title', 'Категория ' . $category->name)

@section('content')
{{--    @php--}}
{{--    $urlArr = [];--}}
{{--    @endphp--}}
{{--    @foreach($category->books as $book)--}}
{{--    {{array_push($urlArr,route('basket-add', ['book' => $category->books->find($book)]))}}--}}
{{--    @endforeach--}}
{{--    @csrf--}}
{{--@dd(csrf_token())--}}
    <div id="v-category">
        <v-category code="{{$category->code}}" csrf="{{csrf_token()}}"/>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
@endsection
{{--@php(echo $urlArr)--}}
{{--urlArr="{{json_encode($urlArr)}}"--}}
{{--<input type="hidden" name="_token" value="C9WTD7mHSx08kgdOK9VrUMSN1IWpFnDcrXVv3GZB">--}}