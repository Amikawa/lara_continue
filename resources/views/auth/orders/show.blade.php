@extends('auth.layouts.master')

@section('title', 'Бронь ')

@section('content')

    <div class="py-4">
        <div class="container">
            <div class="justify-content-center">
                <div class="panel">
                    <h1>Номер брони №{{ $order->id }}</h1>
                    <p>Пользователь: <b>{{ $order->name }}</b></p>
                    <p>Номер теелфона: <b>{{ $order->phone }}</b></p>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Статус</th>
                            <th>Дата сгорания/возврата книг</th>
                            <th>Количество книг</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($order->books()->withTrashed()->get() as $book)
                            <tr>
                                <td>
                                    <a href="{{route('book', [$book->category->code, $book->code])}}">
                                        <img height="56px"
                                             src="{{Storage::url($book->image) }}">
                                        {{ $book->name }}
                                    </a>
                                </td>
                                <td><span class="badge badge-primary">{{$order->status_name}}</span></td>
                                <td>{{ $order->expired_at }} .</td>
                                <td>{{ $book->pivot->count}} .</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection
