@extends('auth.layouts.master')

@section('title', 'Мои заказы')

@section('content')


    {{--@if(auth()->user()->role == 'user')--}}
    {{--    {{dump('Location: '. route('orders-private'))}}--}}
    {{--    {{ redirect()->route('orders-private')}}--}}
    {{--    {{header('Location: '. route('orders-private'))}}--}}

    {{--@else--}}
    @if(isset($reserved) && $reserved->count() > 0)
        <div class="col-md-12">
            <h1>Мои <span class="text-warning">забронированные</span> заказы</h1>
            <table class="table">
                <tbody>
                <tr>
                    <th>
                        Id
                    </th>

                    <th>
                        Когда забронировано
                    </th>
                    <th>
                        Бронь/Вернуть до:
                    </th>


                    <th>
                        Действия
                    </th>
                </tr>

                @foreach($reserved as $order)

                    <tr>
                        <td>{{ $order->id}}</td>
                        <td>{{ $order->created_at->format('H:i d/m/Y') }}</td>
                        <td>{{ $order->expired_at->format('H:i d/m/Y') }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <a class="btn btn-success" type="button"
                                   href="{{route('orders-show', $order)}}">Открыть</a>
                                <a class="btn btn-danger" type="button"
                                   href="{{route('orders-deleteorder', $order)}}">Удалить</a>

                            </div>

                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <h2 class="mb-5 text-muted">У вас отсутствуют зарезервированные книги.</h2>
    @endif
    <br>
    @if(isset($gotten) && $gotten->count() > 0)
        <div class="col-md-12">
            <h1>Мои <span class="text-warning">полученные</span> заказы</h1>
            <table class="table">
                <tbody>
                <tr>
                    <th>
                        Id
                    </th>

                    <th>
                        Всего полученных книг
                    </th>
                    <th>
                        Бронь/Вернуть до:
                    </th>


                    <th>
                        Действия
                    </th>
                </tr>

                @foreach($gotten as $order)

                    <tr>
                        <td>{{ $order->id}}</td>

                        <td>{{ $order->totalBooks() }}</td>
                        <td>{{ $order->expired_at->format('H:i d/m/Y') }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <a class="btn btn-success" type="button"
                                   href="{{route('orders-show', $order)}}">Открыть</a>


                            </div>

                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <h2 class="text-muted">У вас отсутствуют полученные книги.</h2>
    @endif


@endsection



