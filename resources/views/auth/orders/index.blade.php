@extends('auth.layouts.master')

@section('title', 'Забронированные')

@section('content')

    {{--@if(auth()->user()->role == 'user')--}}
    {{--    {{dump('Location: '. route('orders-private'))}}--}}
    {{--    {{ redirect()->route('orders-private')}}--}}
    {{--    {{header('Location: '. route('orders-private'))}}--}}

    {{--@else--}}


    @if(isset($reserved) && $reserved->count() > 0)
        <div class="col-md-12">

            <h1>Бронь</h1>
            <table class="table">
                <tbody>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Имя
                    </th>
                    <th>
                        Телефон
                    </th>
                    <th>
                        Когда забронировано
                    </th>
                    <th>
                        Бронь до:
                    </th>


                    <th>
                        Действия
                    </th>
                </tr>
                @foreach($reserved as $order)
                    <tr>
                        <td>{{ $order->id}}</td>
                        <td>{{ $order->name }}</td>
                        <td>{{ $order->phone }}</td>
                        <td>{{ $order->created_at->format('H:i d/m/Y') }}</td>
                        <td>{{ $order->expired_at->format('H:i d/m/Y') }}</td>


                        <td>
                            <div class="btn-group" role="group">
                                <a class="btn btn-success" type="button"
                                   href="{{route('orders-show', $order)}}">Открыть</a>
                                @role('librarian')
                                <a class="btn btn-primary" type="button"
                                   href="{{route('orders-give', $order)}}">Выдано</a>
                                @endrole
                            </div>

                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <h2 class="mb-5 text-muted"> Отсутствуют зарезервированные книги.</h2><br>
    @endif
    <br><br>
    @if(isset($gotten) && $gotten->count() > 0)
        <div class="col-md-12">
            <h1>Выданные Книги</h1>
            <table class="table">
                <tbody>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Имя
                    </th>
                    <th>
                        Телефон
                    </th>
                    <th>
                        Вернуть до:
                    </th>


                    <th>
                        Действия
                    </th>
                </tr>

                @foreach($gotten as $order)
                    <tr>
                        <td>{{ $order->id}}</td>
                        <td>{{ $order->name }}</td>
                        <td>{{ $order->phone }}</td>
                        <td>{{ $order->expired_at->format('H:i d/m/Y') }}</td>


                        <td>
                            <div class="btn-group" role="group">
                                <a class="btn btn-success" type="button"
                                   href="{{route('orders-show', $order)}}">Открыть</a>
                                @role('librarian')
                                <a class="btn btn-primary" type="button"
                                   href="{{route('orders-accept', $order)}}">Принять</a>
                                @endrole
                            </div>

                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <h2 class="text-muted">Отсутствуют полученные книги.</h2>
    @endif

@endsection
