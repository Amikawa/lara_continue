@extends('auth.layouts.master')

@section('title', 'Пользователи')

@section('content')
    <div class="col-md-12">
        <h1>Пользователи</h1>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    #
                </th>
                <th>
                    Имя
                </th>
                <th>
                    Почта
                </th>
                <th>
                    Роль
                </th>
                <th>
                    Действия
                </th>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role }}</td>
                    <td>
                        <div class="btn-group" role="group">
                            <form action="{{ route('users.destroy', $user) }}" method="POST">
                                <a class="btn btn-success" type="button"
                                   href="{{ route('users.show', $user) }}">Открыть</a>
                                <a class="btn btn-warning" type="button" href="{{ route('users.edit', $user) }}">Редактировать</a>
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-danger" type="submit" value="Удалить"></form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$users->withQueryString()->links()}}
        <a class="btn btn-success" type="button"
           href="{{ route('users.create') }}">Добавить пользователя</a>
    </div>
@endsection
