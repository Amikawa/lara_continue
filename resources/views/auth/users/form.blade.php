@extends('auth.layouts.master')

@isset($user)
    @section('title', 'Редактировать пользователя ' . $user->name)
@else
    @section('title', 'Создать пользователя')
@endisset

@section('content')
    <div class="col-md-12">
        @isset($user)
            <h1>Редактировать Пользователя <b>{{ $user->name }}</b></h1>
        @else
            <h1>Добавить Пользователя</h1>
        @endisset
        <br>
        <br>
        <form method="POST" enctype="multipart/form-data"
              @isset($user)
              action="{{ route('users.update', $user) }}"
              @else
              action="{{ route('users.store') }}"
                @endisset
        >
            <div>
                @isset($user)
                    @method('PUT')
                @endisset
                @csrf
                <div>
                    @error('name')
                    <div class="alert alert-warning">{{ $message }}</div>
                    @enderror

                    <div class="input-group row">
                        <label for="name" class="col-sm-4 col-form-label">Имя: </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control-input" name="name" id="name"
                                   value="{{old('name', isset($user)?  $user->name : null) }}">
                        </div>
                    </div>

                    <br>
                    <br>


                    @error('email')
                    <div class="alert alert-warning">{{ $message }}</div>
                    @enderror

                    <div class="input-group row">
                        <label for="email" class="col-sm-4 col-form-label">почта: </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control-input" name="email" id="email"
                                   value="{{old('email', isset($user)?  $user->email : null )}}">
                        </div>
                    </div>

                    <br>
                    <br>

                    @isset($user)
                    @else
                        @error('password')
                        <div class="alert alert-warning">{{ $message }}</div>
                        @enderror
                        <div class="input-group row">
                            <label for="password" class="col-sm-4 col-form-label">пароль: </label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control-input" name="password" id="password"
                                       value="{{old('password', null )}}">
                            </div>
                        </div>
                    @endisset


                    <br>
                    <br>

                    <div>
                        <div class="input-group">
                            <label for="role">Example select</label>
                            <select name="role" class="form-control" id="role">
                                <option value="1">Администратор</option>
                                <option value="2">Библиотекарь</option>
                                <option value="3">Пользователь</option>

                            </select>
                        </div>
                    </div>


                    <br>

                    <br>

                    <button class="btn btn-success">Сохранить</button>

        </form>
    </div>
@endsection

