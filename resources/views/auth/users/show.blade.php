@extends('auth.layouts.master')

@section('title', 'Пользователь ' . $user->name)

@section('content')

    <h1>{{$user->name}}</h1>

    <table class="table">
        <tbody>
        <tr>
            <th>
                Поле
            </th>
            <th>
                Значение
            </th>
        </tr>
        <tr>
            <td>ID</td>
            <td>{{ $user->id }}</td>
        </tr>
        <tr>
            <td>Mail</td>
            <td>{{ $user->email }}</td>
        </tr>
        <tr>
            <td>Роль</td>
            <td>{{ $user->role }}</td>
        </tr>


        <tr>
            <td>Кол-во товаров</td>

            <td>{{ $user->orders()->count() }}</td>
        </tr>
        </tbody>
    </table>

    <form method="POST" action="{{route('users.password', $user->id)}}">

        <div class="input-group row">
            <div class="col-md-4 offset-md-4">
                <label for="password" class="col-sm-4 col-form-label">Пароль: </label>
                <input type="text" class="form-control-input" name="password" id="password"
                       value="{{ $user->name }}">

                <br>
                @csrf
                <button class="btn btn-primary" type="submit">Изменить пароль</button>
            </div>
        </div>
        <p>@error('password')
            {{$message}}
            @enderror</p>


    </form>
    </div>
@endsection
