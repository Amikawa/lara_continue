<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Админка: @yield('title')</title>

    <!-- Scripts -->
    <script src="/js/app.js" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    {{--    <link href="/css/admin.css" rel="stylesheet">--}}
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ route('index') }}">
                Вернуться на сайт
            </a>

            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    @role('admin')
                    <li class="mr-2"><a href="{{route('users.index')}}">Пользователи </a></li>
                    @endrole

                    @role('librarian')
                    <li class="mr-2"><a href="{{route('books.index')}}">Книги </a></li>
                    <li class="mr-2"><a href="{{route('orders-orders')}}">Забронированные </a></li>
                    <li class="mr-2"><a href="{{route('authors.index')}}">Авторы </a></li>
                    @endrole
                    @auth

                        @foreach(auth()->user()->roles as $rolee )
                            <li class="mr-2"><a class="text-success"><b>{{$rolee}}</b></a></li>
                        @endforeach
                        <li class="mr-2"><a href="{{route('orders-private')}}">Мои заказы</a></li>
                </ul>
                @endauth
                @guest
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">Войти</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Зарегистрироваться</a>
                        </li>
                    </ul>
                @endguest

                @auth

                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false" v-pre>
                                Аккаунт
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout')}}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Выйти
                                </a>

                                <form id="logout-form" action="{{ route('logout')}}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                @endauth
            </div>
        </div>
    </nav>

    <div class="py-4">

        <div class="container">
            @if(session()->has('success'))
                <p class="alert alert-success">{{ session()->get('success') }}</p>
            @endif
            @if(session()->has('warning'))
                <p class="alert alert-success">{{ session()->get('warning') }}</p>
            @endif
            <p class="text-dark">Здраствуйте, <span
                        class="text-secondary text-capitalize">{{auth()->user()->name}}</span></p><br>
            <p class="text-dark">Ваша роль - <span
                        class="text-secondary text-capitalize">{{auth()->user()->role}}</span></p>
            <div class="row justify-content-center">

                @yield('content')
            </div>
        </div>
    </div>
</div>
</body>
</html>
