@extends('auth.layouts.master')

@isset($author)
    @section('title', 'Редактировать автора ' . $author->name)
@else
    @section('title', 'Создать автора')
@endisset

@section('content')
    <div class="col-md-12">
        @isset($author)
            <h1>Редактировать автора <b>{{ $author->first_name }}</b></h1>
        @else
            <h1>Добавить автора</h1>
        @endisset
        <br>
        <br>
            <form method="POST" enctype="multipart/form-data"
                  @isset($author)
                  action="{{ route('authors.update', $author) }}"
                  @else
                  action="{{ route('authors.store') }}"
                @endisset
            >
                <div>
                    @isset($author)
                        @method('PUT')
                    @endisset
                    @csrf
                    <div class="form-group">

                        <div class="input-group">
                            @error('first_name')
                            <div class="alert alert-warning">{{ $message }}</div>
                            @enderror
                            <label for="first_name" class="col-form-label">Имя: </label>
                            <input type="text" class="form-control-input" name="first_name" id="first_name"
                                   value="{{old('first_name', isset($author)?  $author->first_name : null) }}">
                        </div>

                        <br>
                        <br>
                        <div class="input-group">
                            @error('second_name')
                            <div class="alert alert-warning">{{ $message }}</div>
                            @enderror

                            <label for="second_name" class="col-form-label">Описание: </label>

                            <input type="text" class="form-control-input" name="second_name" id="second_name"
                                   value="{{old('second_name', isset($author)?  $author->second_name : null) }}">
                        </div>
                        <br>
                        <br>

                        <div class="input-group">

                            <label for="publisher">Выбрать</label>
                            <select name="publisher_id" class="form-control" id="publisher">
                                <option value="1">Петербужское издание</option>
                                <option value="2">Казанское издание</option>
                                <option value="3">Московское издание</option>
                            </select>
                        </div>


                        <br>
                        <br>
                        <div class="input-group">
                            <label class="btn btn-default btn-file">
                                Загрузить <input type="file" style="display: none;" name="image" id="image">
                            </label>
                        </div>
                        <br>

                        <br>

                        <button class="btn btn-success">Сохранить</button>
                    </div>
                </div>
            </form>
    </div>
@endsection

