@extends('auth.layouts.master')

@section('title', 'автор ' . $author->name)

@section('content')

    <h1>{{$author->name}}</h1>

    <table class="table">
        <tbody>
        <tr>
            <th>
                Поле
            </th>
            <th>
                Значение
            </th>
        </tr>
        <tr>
            <td>ID</td>
            <td>{{ $author->id }}</td>
        </tr>
        <tr>
            <td>Имя</td>
            <td>{{ $author->first_name }}</td>
        </tr>
        <tr>
            <td>Фамилия</td>
            <td>{{ $author->second_name }}</td>
        </tr>


        <tr>
            <td>Издательство</td>

            <td>{{ $author->publisher->name }}</td>
        </tr>
        <tr>
            <td>Картинка</td>
            <td><img src="{{Storage::url($author->image)}}"
                     height="240px"></td>
        </tr>
        </tbody>
    </table>





    </div>
@endsection
