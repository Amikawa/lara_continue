@extends('auth.layouts.master')

@section('title', 'авторы')

@section('content')
    <div class="col-md-12">
        <h1>авторы</h1>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    Id
                </th>
                <th>
                    Имя
                </th>
                <th>
                    Фамилия
                </th>
                <th>
                    Издательство
                </th>
                <th>
                    Фотография
                </th>

            </tr>
            @foreach($authors as $author)
                <tr>
                    <td>{{ $author->id }}</td>
                    <td>{{ $author->first_name }}</td>
                    <td>{{ $author->second_name }}</td>
                    <td>{{ $author->publisher->name }}</td>
                    <td><img src="{{ Storage::url($author->image)}}"></td>
                    <td>
                        <div class="btn-group" role="group">
                            <form action="{{ route('authors.destroy', $author) }}" method="POST">
                                <a class="btn btn-success" type="button" href="{{ route('authors.show', $author) }}">Открыть</a>
                                <a class="btn btn-warning" type="button" href="{{ route('authors.edit', $author) }}">Редактировать</a>
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-danger" type="submit" value="Удалить"></form>
                        </div>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
        {{$authors->withQueryString()->links()}}
        <a class="btn btn-success" type="button"
           href="{{ route('authors.create') }}">Добавить автора</a>
    </div>
@endsection
