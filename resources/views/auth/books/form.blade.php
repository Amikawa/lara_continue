@extends('auth.layouts.master')

@isset($book)
    @section('title', 'Редактировать книгу ' . $book->name)
@else
    @section('title', 'Создать книгу')
@endisset

@section('content')
    <div class="col-md-12">
        @isset($book)
            <h1>Редактировать Книгу <b>{{ $book->name }}</b></h1>
        @else
            <h1>Добавить Книгу</h1>
        @endisset
        <br>
        <br>
        <form method="POST" enctype="multipart/form-data"
              @isset($book)
              action="{{ route('books.update', $book) }}"
              @else
              action="{{ route('books.store') }}"
                @endisset
        >
            <div>
                @isset($book)
                    @method('PUT')
                @endisset
                @csrf
                <div class="form-group">

                    <div class="input-group">
                        @error('name')
                        <div class="alert alert-warning">{{ $message }}</div>
                        @enderror
                        <label for="name" class="col-form-label">Имя: </label>
                        <input type="text" class="form-control-input" name="name" id="name"
                               value="{{old('name', isset($book)?  $book->name : null) }}">
                    </div>

                    <br>
                    <br>


                    <div class="input-group">

                        <label for="category">Example select</label>
                        <select name="category_id" class="form-control" id="category">
                            <option value="1">Фантастика</option>
                            <option value="2">Классика</option>
                            <option value="3">Манга</option>

                        </select>
                    </div>

                    <br>
                    <br>


                    <div class="input-group">
                        @error('description')
                        <div class="alert alert-warning">{{ $message }}</div>
                        @enderror

                        <label for="description" class="col-form-label">Описание: </label>

                        <input type="text" class="form-control-input" name="description" id="description"
                               value="{{old('description', isset($book)?  $book->description : null) }}">
                    </div>


                    <br>
                    <div class="input-group">
                        @error('count')
                        <div class="alert alert-warning">{{ $message }}</div>
                        @enderror

                        <label for="count" class="col-form-label">Количество: </label>

                        <input type="text" class="form-control-input" name="count" id="count"
                               value="{{old('count', isset($book)?  $book->count : null) }}">
                    </div>
                    <br>
                    <br>


                    <div class="input-group">
                        @error('code')
                        <div class="alert alert-warning">{{ $message }}</div>
                        @enderror

                        <label for="code" class=" col-form-label">Код книги для Url: </label>

                        <input type="text" class="form-control-input" name="code" id="code"
                               value="{{old('code', isset($book)?  $book->code : null) }}">
                    </div>


                    <br>


                    <div class="input-group">


                        <label class="btn btn-default btn-file">
                            Загрузить <input type="file" style="display: none;" name="image" id="image">
                        </label>

                    </div>
                    <br>

                    <br>

                    <button class="btn btn-success">Сохранить</button>
                </div>
        </form>
    </div>
@endsection

