@extends('auth.layouts.master')

@section('title', 'книги')

@section('content')
    <div class="col-md-12">
        <h1>книги</h1>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    Id
                </th>
                <th>
                    Название
                </th>
                <th>
                    Кол-во
                </th>
                <th>
                    Описание
                </th>
                <th>
                    Uri код пути
                </th>

            </tr>
            @foreach($books as $book)
                <tr>
                    <td>{{ $book->id }}</td>
                    <td>{{ $book->name }}</td>
                    <td>{{ $book->count }}</td>
                    <td>{{ $book->description }}</td>
                    <td>{{ $book->code }}</td>
                    <td>
                        <div class="btn-group" role="group">
                            <form action="{{ route('books.destroy', $book) }}" method="POST">
                                <a class="btn btn-success" type="button"
                                   href="{{ route('books.show', $book) }}">Открыть</a>
                                <a class="btn btn-warning" type="button" href="{{ route('books.edit', $book) }}">Редактировать</a>
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-danger" type="submit" value="Удалить"></form>
                        </div>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
        {{$books->withQueryString()->links()}}
        <a class="btn btn-success" type="button"
           href="{{ route('books.create') }}">Добавить книгу</a>
    </div>
@endsection
