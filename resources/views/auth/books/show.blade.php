@extends('auth.layouts.master')

@section('title', 'книга ' . $book->name)

@section('content')

    <h1>{{$book->name}}</h1>

    <table class="table">
        <tbody>
        <tr>
            <th>
                Поле
            </th>
            <th>
                Значение
            </th>
        </tr>
        <tr>
            <td>ID</td>
            <td>{{ $book->id }}</td>
        </tr>
        <tr>
            <td>Имя</td>
            <td>{{ $book->name }}</td>
        </tr>
        <tr>
            <td>Категория</td>
            <td>{{ $book->category->name }}</td>
        </tr>


        <tr>
            <td>Описание</td>

            <td>{{ $book->description }}</td>
        </tr>
        <tr>
            <td>Картинка</td>
            <td><img src="{{Storage::url($book->image)}}"
                     height="240px"></td>
        </tr>
        </tbody>
    </table>





    </div>
@endsection
