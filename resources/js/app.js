require('./bootstrap');
import Vue from 'vue';
import store from './store';
// import router from "./router";
// spa на php, ибо весь проект придётся переписывать, если нужен на js, тк изначально задание было на беке.


Vue.component('VCategories', require('./views/VCategories').default);
Vue.component('VCategory', require('./views/VCategory').default);
Vue.component('Loader', require('./components/app/Loader').default);



const categories = new Vue({store}).$mount('#v-categories');
const category = new Vue({store}).$mount('#v-category');
