import Vue from 'vue';
import Vuex from 'vuex';
import category from './category/index';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ERROR: null,
  },
  mutations: {
    SET_ERROR(state, error) {
      state.ERROR = error
    },
    CLEAR_ERROR(state) {
      state.ERROR = null
    }
  },

  getters: {
    ERROR: s => s.ERROR,
  },
  modules: {
    category
  }
});

