import axios from "axios";

export default {
    async FETCH_CATEGORIES({commit}) {
        // Забрать категории
        try {
            let categories;
            let res = await axios.get('/api/categories')

            categories = res.data.data;
            commit('SET_CATEGORIES', categories);
            return categories;


        } catch (err) {
            commit('SET_ERROR', err)
        }
    },
    async FETCH_CATEGORY({commit}, code) {
        // Забрать одну категорию по её коду
        try {
            let category;
            let res = await axios.get(`/api/${code}`);
            category = res.data.data;
            return category;
        } catch (err) {
            commit('SET_ERROR', err)
        }
    }
};
