<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

    Route::get('/', 'MainController@index')->name('index');
//    Route::get('/home', 'HomeController@index')->name('home');
    Route::view('/categories', 'categories')->name('categories');

    Route::middleware('auth')->group(
        function ()
        {
            Route::group([
                'as' => 'orders-',
                'namespace' => 'Admin'
             ], function()
             {
                 Route::get('/acceptorder/{order}', 'OrderController@acceptOrder')->name('accept');
                 Route::get('/give/{order}', 'OrderController@giveOrder')->name('give');
                 Route::get('/show/{order}', 'OrderController@show')->name('show');
                 Route::get('/private', 'OrderController@private')->name('private')->middleware('hasOrder');
                 Route::get('/orders', 'OrderController@index')->name('orders');
                 Route::get('/deleteorder/{order}', 'OrderController@deleteOrder')->name('deleteorder');

             });

            Route::get('/logout', 'Auth\LoginController@logout')->name('get-logout');
            Route::group(['middleware' => 'role:librarian'],
                function ()
                {
                    Route::group(['prefix' => 'books'],
                        function ()
                        {
                            Route::resources([
                            'books' => 'BookController',
                            'authors' => 'AuthorController',
                            ]);
                        });
                });

            Route::group(['middleware' => 'role:admin'],
                function ()
                {


                    Route::group([
                        'prefix'    => 'admin',
                        'namespace' => 'Admin',
                    ], function ()
                        {
                        Route::post('/password/{userId}',
                            'UserController@password')
                            ->name('users.password');
                        Route::resource('users', 'UserController');
                        });
                });

        });




    Route::group([
        'prefix' => 'basket',
        'as' => 'basket'
    ], function ()
    {
        Route::post('/add/{book}', 'BasketController@basketAdd')->name('-add');
        Route::group([
            'middleware' => 'BasketNotEmpty',

        ], function ()
            {
                Route::get('/', 'BasketController@basket')->name('');
                Route::get('/place', 'BasketController@basketPlace')->name('-place');
                Route::post('/remove/{book}', 'BasketController@basketRemove')->name('-remove');
                Route::post('/place', 'BasketController@basketConfirm')->name('-confirm');
            });
    });

    Auth::routes();

    Route::get('/{categoryCode}', function ($categoryCode)
    {
        $categoryCode = \App\Models\Category::categoryByCode($categoryCode)->first();
        return view('category', ['category' => $categoryCode]);
    })->name('category');

    Route::get('/{categoryCode}/{bookCode}', 'MainController@book')->name('book');



