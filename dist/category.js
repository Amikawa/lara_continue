"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  state: {
    CATEGORIES: {}
  },
  getters: {
    CATEGORIES: function CATEGORIES(s) {
      return s.categories;
    }
  },
  mutations: {
    SET_CATEGORIES: function SET_CATEGORIES(s, categories) {
      s.CATEGORIES = categories;
    }
  },
  actions: {
    FETCH_CATEGORIES: function FETCH_CATEGORIES(_ref) {
      var commit = _ref.commit;

      try {
        var categories;

        _axios["default"].get('/api/categories').then(function (response) {
          categories = response.data.data;
          commit('SET_CATEGORIES', categories);
        })["catch"](function (err) {
          throw err;
        });
      } catch (err) {
        commit('SET_ERROR', err);
      }
    }
  }
}; // async fetchCategories({commit, dispatch}) {
//   try {
//     const uid = await dispatch('getUid')
//     const categories = (await firebase.database().ref(`/users/${uid}/categories`).once('value')).val() || {}
//     return Object.keys(categories).map(key => ({...categories[key], id: key}))
//   } catch (e) {
//     commit('setError', e)
//     throw e
//   }
// },
// async fetchCategoryById({commit, dispatch}, id) {
//   try {
//     const uid = await dispatch('getUid')
//     const category = (await firebase.database().ref(`/users/${uid}/categories`).child(id).once('value')).val() || {}
//     return {...category, id}
//   } catch (e) {
//     commit('setError', e)
//     throw e
//   }
// },
// async updateCategory({commit, dispatch}, {title, limit, id}) {
//   try {
//     const uid = await dispatch('getUid')
//     await firebase.database().ref(`/users/${uid}/categories`).child(id).update({title, limit})
//   } catch (e) {
//     commit('setError', e)
//     throw e
//   }
// },
// async createCategory({commit, dispatch}, {title, limit}) {
//   try {
//     const uid = await dispatch('getUid')
//     const category = await firebase.database().ref(`/users/${uid}/categories`).push({title, limit})
//     return {title, limit, id: category.key}
//   } catch (e) {
//     commit('setError', e)
//     throw e
//   }
// }
//     }
// }

exports["default"] = _default;
//# sourceMappingURL=category.js.map