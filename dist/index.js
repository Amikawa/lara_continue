"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _vuex = _interopRequireDefault(require("vuex"));

var _index = _interopRequireDefault(require("./category/index"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_vue["default"].use(_vuex["default"]);

var _default = new _vuex["default"].Store({
  state: {
    ERROR: null
  },
  mutations: {
    SET_ERROR: function SET_ERROR(state, error) {
      state.ERROR = error;
    },
    CLEAR_ERROR: function CLEAR_ERROR(state) {
      state.ERROR = null;
    }
  },
  getters: {
    ERROR: function ERROR(s) {
      return s.ERROR;
    }
  },
  modules: {
    category: _index["default"]
  }
});

exports["default"] = _default;
//# sourceMappingURL=index.js.map