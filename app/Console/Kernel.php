<?php

namespace App\Console;

use App\Mail\ExpiredOrder;
use App\Mail\LastDayOrder;
use App\Models\Order;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands
        = [
            //
        ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        // вбиваем дату сгорания ордера
        // $order->expired_at = now()->addDays(3);
        // нужно каждый день сравнивать expired_at и now->isCurrentDay()
        $schedule->call(
            function () {
                $reserved = Order::reserved()->get();
                foreach ($reserved as $order) {
                    if ($order->expired_at->isCurrentDay()) {
                        $order->delete();
                        Mail::to($order->user->email)->send(
                            new ExpiredOrder($order)
                        );
                    }
                }

                $gotten = Order::gotten()->get();
                foreach ($gotten as $got) {
                    if ($got->expired_at->subDay()->isCurrentDay()) {
                        //отправить маил пользователю, что сегодня последний день проката,
                        // пусть занесёт книгу
                        Mail::to($got->user->email)->send(
                            new ExpiredOrder($got)
                        );
                    }
                }
            }
        )->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
