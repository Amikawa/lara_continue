<?php

namespace App\Classes;

use App\Models\Order;
use App\Models\Book;
use Illuminate\Support\Facades\Auth;

class Basket
{
    protected $order;

    /**
     * Basket constructor.
     *
     * @param  bool  $createOrder
     */
    public function __construct($createOrder = false)
    {
//      Забрать с сессии Id Ордера
        $orderId = session('orderId');
//      Если в сессии такого ключа нет, то создаём Ордер и закидываем в сессию
        if (is_null($orderId) && $createOrder) {
            $data = [];
            if (Auth::check()) {
                $data['user_id'] = Auth::id();
            }

            $this->order = Order::create($data);
            session(['orderId' => $this->order->id]);
        } else { // иначе забираем по ключу с сессии Ордер с бд
            $this->order = Order::findOrFail($orderId);
        }
    }

    /**
     * @return mixed
     * Геттер к Ордеру
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return $this
     *  Зарезервировать книги, увеличивая число reserved в базе и в карточке.
     */
    public function reserveBooks()
    {
        $this->order->expired_at = now()->addDays(3);
        // нужно каждый день сравнивать expired_at и now->isCurrentDay()
        foreach ($this->order->books as $book) {
            $count = $book->pivot->count;
            $book->reserved += $count;
            $book->save();
        }
        return $this;
    }

    /**
     * @return bool
     * // Проверяем, чтобы доступных книг было не меньше, чем добавляется.
     */
    public function countAvailable()
    {
        foreach ($this->order->books as $book) {
            if ($book->total < $this->getPivotRow($book)->count) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $name имя того, кто бронирует
     * @param $phone телефон
     *
     * @return false
     * Здесь мы изменяем статус ордера с 0 ( корзина)
     * до 1( занесено в базу данных и зарезервированы книги)
     * Так же заносим необходимые данные тлф и имя
     */
    public function saveOrder($name, $phone)
    {
        if (!$this->countAvailable()) {
            return false;
        }
        return $this->order->saveOrder($name, $phone);
    }

    /**
     * @param $book
     *
     * @return mixed
     *
     * Заходим в промежуточную таблицу Ордер - Book
     * Для того, чтобы смочь забрать оттуда
     * Кол-во конкретных книг с конкретного Ордера
     */
    protected function getPivotRow($book)
    {
        return $this->order->books()->where('book_id', $book->id)->first(
        )->pivot;
    }

    /**
     * @param  Book  $book
     * Минус 1 кол-во книги при нажатии на карточке на кнопку "-"
     */
    public function removeBook(Book $book)
    {
        if ($this->order->books->contains($book)) {
            $pivotRow = $this->getPivotRow($book);
            if ($pivotRow->count < 2) {
                $this->order->books()->detach($book->id);
            } else {
                $pivotRow->count--;
                $pivotRow->update();
            }
        }
    }

    /**
     * @param  Book  $book
     *
     * @return bool
     * Добавляем книгу при нажатии на карточке на "+"
     * Добавляем в промежуточную таблицу кол-во
     * К тому же не можем добавить больше,
     * чем доступно
     */
    public function addBook(Book $book)
    {
        if ($this->order->books->contains($book)) {
            $pivotRow = $this->getPivotRow($book);
            $pivotRow->count++;
            if ($pivotRow->count > $book->total) {
                return false;
            }
            $pivotRow->update();
        } else {
            if ($book->total == 0) {
                return false;
            }
            $this->order->books()->attach($book->id);
        }


        return true;
    }
}
