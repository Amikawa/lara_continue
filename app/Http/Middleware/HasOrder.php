<?php

namespace App\Http\Middleware;

use Closure;

class HasOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty(
            auth()->user()->orders
            && auth()->user()->orders->count() > 0
        )
        ) {
            return $next($request);
        } else {
            return redirect()->back()->with(
                'warning',
                'Сначала оформите бронь.'
            );
        }
    }
}
