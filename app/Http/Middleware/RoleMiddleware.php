<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (is_null($role)) {
            return $next($request);
        }

        // Какая роль у авторизованного пользователя?
        // если как в параметре, то пропускаем дальше
        if (auth()->user()->role == $role) {
            return $next($request);
        } else {
            return response()->make('У вас нет прав');
        }
    }


}
