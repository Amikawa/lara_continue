<?php

namespace App\Http\Middleware;

use App\Models\Order;
use Closure;

class BasketNotEmpty
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $orderId = session('orderId');
        if (!is_null($orderId)) {
            $order = Order::with('books')->find($orderId);
            if ($order->books->count() == 0) {
                session()->flash('warning', 'Выберите книги');
                return redirect()->route('index');
            } else {
                return $next($request);
            }
        }
        return redirect()->route('index')->with('warning', 'Корзина пуста');
    }
//        session()->flash('warning', 'Сначала выберите книгу');
//        redirect()->route('categories');


}
