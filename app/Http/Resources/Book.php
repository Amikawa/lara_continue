<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Book extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'category_id' => $this->category_id,
            'description' => $this->description,
            'image' => $this->image,
            'count' => $this->count,
            'total' => $this->total,
            'reserved' => $this->reserved,
            'author_id' => $this->author_id,
            'available' => $this->isAvailable(),
            'category' => $this->category->code
        ];
    }
}
