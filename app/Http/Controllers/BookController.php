<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * REST ПО КНИГАМ
     */
    public function index()
    {
        $books = Book::paginate(6);
        return view('auth.books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.books.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $data = $request->all();
        unset($data['image']);
        if ($request->has('image')) {
            $data['image'] = $request->file('image')->store('books');
        }
        $stored = Book::create($data);
        return redirect()->route('books.index')->with(
            'success',
            'книга добавлена'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('auth.books.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view('auth.books.form', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     *
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, Book $book)
    {
        $params = $request->all();
        unset($params['image']);
        if ($request->has('image')) {
            Storage::delete($book->image);
            $params['image'] = $request->file('image')->store('books');
        }
        $book->update($params);
        return redirect()->route('books.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect()->route('books.index')->with(
            'success',
            'книга удалена'
        );
    }
}
