<?php

namespace App\Http\Controllers;

use App\Classes\Basket;
use App\Http\Requests\BasketPlaceRequest;
use App\Models\Order;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasketController extends Controller
{
    /**
     * При добавлении книги в корзину
     * появляется эта страница, те, корзина,
     * так же для неё есть отдельный роут.
     */
    public function basket()
    {
        $order = (new Basket())->getOrder();
        return view('basket', compact('order'));
    }

    /**
     * Бронирование ордера и изменение его статуса с 0 на 1.
     */
    public function basketConfirm(BasketPlaceRequest $request)
    {
        if (($order = new Basket())->reserveBooks()->saveOrder(
            $request->name,
            $request->phone
        )
        ) {
            session()->flash(
                'success',
                'Ваш заказ принят в обработку!  У вас есть 3 дня, чтобы забрать ваш заказ с книгами.'
            );
        } else {
            session()->flash('warning', 'Случилась ошибка');
        }
        return redirect()->route('index');
    }

    /**
     * @param  BasketPlaceRequest  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * ПОдтвердление корзины пользователем,
     * и валидация его тлфа и имени, редиректим на страницу
     * подтверждения оформления брони
     */
    public function basketPlace()
    {
        $basket = new Basket();
        $order = $basket->getOrder();
        if (!$basket->countAvailable()) {
            session()->flash(
                'warning',
                'Книга не доступна для заказа в полном объеме'
            );
            return redirect()->route('basket');
        }
        return view('order', compact('order'));
    }

    /**
     * @param  Book  $book
     *
     * @return \Illuminate\Http\RedirectResponse
     * Добавление книги в Корзину
     */
    public function basketAdd(Book $book)
    {
        $result = (new Basket(true))->addBook($book);

        if ($result) {
            session()->flash('success', 'Добавленa книга '.$book->name);
        } else {
            session()->flash(
                'warning',
                'Книга '.$book->name.' в большем кол-ве не доступен'
            );
        }
        return redirect()->route('basket');
    }

    /**
     * @param  Book  $book
     *
     * @return \Illuminate\Http\RedirectResponse
     * Удалении книги с корзины
     */
    public function basketRemove(Book $book)
    {
        (new Basket())->removeBook($book);

        session()->flash('warning', 'Удалена книга  '.$book->name);

        return redirect()->route('basket');
    }
}
