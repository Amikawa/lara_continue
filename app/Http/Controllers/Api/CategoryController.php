<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\Category;
use App\Models\Category as CategoryModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @return CategoryCollection
     * Апи списка категорий
     */
    public function index()
    {
        return new CategoryCollection(CategoryModel::all());
    }

    /**
     * @param $code
     *
     * @return Category
     * Апи для конкретной категории
     */
    public function category($code)
    {
        return new Category(CategoryModel::categoryByCode($code)
                                ->first()->load('books'));
    }


}
