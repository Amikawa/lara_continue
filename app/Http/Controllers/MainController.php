<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Book;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Models\Order;


class MainController extends Controller
{
    /**
     * @param  Request  $request
     *
     * Главная страница Библиотеки
     * с фильтром и пагинацией и тд
     */
    public function index(Request $request)
    {
        $booksQuery = book::with(['category', 'author.publisher']);

        if ($request->filled('category')) {
            $GLOBALS['categoryName'] = $request->category;
            $booksQuery->whereHas(
                'category',
                function ($query) {
                    $query->where('name', $GLOBALS['categoryName']);
                }
            );
            unset($GLOBALS['categoryName']);
        }

        if ($request->filled('author')) {
            $GLOBALS['authorName'] = $request->author;
            $booksQuery->whereHas(
                'author',
                function ($query) {
                    $query->where('first_name', $GLOBALS['authorName']);
                    $query->orWhere('second_name', $GLOBALS['authorName']);
                    $query->orWhere('full_name', $GLOBALS['authorName']);
                }
            );
            unset($GLOBALS['authorName']);
        }

        if ($request->filled('book')) {
            $booksQuery->where('name', 'like', '%'.$request->book.'%');
        }

        $books = $booksQuery->paginate(6);
//        dd(get_class_methods($this));
        return view('index', compact('books'));
    }

//  ---------------------UPD:   СОЗДАНО ЧЕРЕЗ VUEjs  -------------------------
//    public function categories()
//    {
//        $categories = Category::get();
//        return view('categories', compact('categories'));
//    }

//    public function category($code)
//    {
//        $category = Category::categoryByCode($code)->first();
//        return view('category', compact('category'));
//    }
// ----------------------------------------------------------------------------

    /**
     * @param $categoryCode
     * @param $bookCode
     *
     * Страница детальной инфы о книге
     */
    public function book($categoryCode, $bookCode)
    {
        $book = Book::withTrashed()->byCode($bookCode)->first();
        return view('book', compact('book'));
    }
}
