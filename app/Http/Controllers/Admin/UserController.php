<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePassRequest;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * Rest пользователи на админке
     */
    public function index()
    {
        $users = User::paginate(6);
        return view('auth.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.users.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            request(),
            [
                'name'     => 'required',
                'email'    => 'required|email',
                'password' => 'required'
            ]
        );
//        $data = $request;
//        $data['password'] = bcrypt($request['password']);
//        User::create($data->all());
//        return redirect()->route('users.index');
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        return redirect()->route('users.index')->with(
            'success',
            "Пользователь добавлен"
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('auth.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('auth.users.form', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate(
            request(),
            [
                'name'  => 'required',
                'email' => 'required|email',
            ]
        );
        $data = $request->all();
//        $data['password'] = bcrypt($data['password']);
        $user->update($data);
        return redirect()->route('users.index')->with(
            'success',
            "Пользователь {$user->name} изменен"
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users-index');
    }

    /**
     * @param  ChangePassRequest  $request
     * @param  User  $user
     *
     * @return \Illuminate\Http\RedirectResponse
     * Это функция по изменению пароля пользователя.
     */
    public function password(ChangePassRequest $request, User $user)
    {
//        $user = User::find($request->route()->parameter('userId'));
//        unset($user['remember_token']);

        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $user->update($data);
        return redirect()->route('orders-orders')
            ->with('success', 'пароль успешно изменён');
    }






//        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
//            // The passwords matches
//            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
//        }
//
//        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
//            //Current password and new password are same
//            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
//        }
//
//        $validatedData = $request->validate([
//            'current-password' => 'required',
//            'new-password' => 'required|string|min:6|confirmed',
//        ]);
//
//        //Change Password
//        $user = Auth::user();
//        $user->password = bcrypt($request->get('new-password'));
//        $user->save();
//
//        return redirect()->back()->with("success","Password changed successfully !");

//    }

}
