<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Список 2 видов ордеров: зарезервированных
     * и тех, которые уже в прокате у человека
     */
    public function index()
    {
        if (auth()->user()->role == 'user') {
            return redirect(route('orders-private'));
        } else {
            $reserved = Order::reserved()->get();
            $gotten = Order::gotten()->get();
            return view('auth.orders.index', compact('reserved', 'gotten'));
        }
    }

    /**
     * @param  Order  $order
     *Открываем определённый Ордер
     * и смотрим, какие в него книги входят
     * и какого типа ордер: зарезервированный
     * или тот, что уже в руках у человека
     *
     */
    public function show(Order $order)
    {
        $user = Auth()->user();
        if ($user->is_user) {
            if ($user->orders->contains($order)) {
                return view('auth.orders.show', compact('order'));
            } else {
                return redirect()->route('orders-private')->with(
                    'warning',
                    'Это не ваши заказы'
                );
            }
        } else {
            return view('auth.orders.show', compact('order'));
        }
    }

    /**
     * Ордеры, привязанному к пользователю,
     * что вошёл в систему, он увидит только
     * свои ордеры, личный кабинет типо.
     */
    public function private()
    {
        // если у чела нет заказов, то мидлваре не пропустит
        $id = auth()->id();
        $reserved = Order::reserved()->byUserId($id)->get();
        $gotten = Order::gotten()->byUserId($id)->get();
        return view('auth.orders.private', compact('reserved', 'gotten'));
    }

    /**
     * @param  Order  $order
     *
     *  Удаляем Ордер
     *  В дальнейшем функционал может расширяться..
     *
     */
    public function deleteOrder(Order $order)
    {
        Order::deleteOrder($order);
        return redirect()->route('orders-orders');
    }

    /**
     * @param  Order  $order
     *
     * @return \Illuminate\Http\RedirectResponse
     * При нажатии библиотекарем на кнопку
     * "Ордер выдан человеку"
     * отправляет письмо пользователю с данными,
     * когда ему следует вернуть книги
     */
    public function giveOrder(Order $order)
    {
        Order::sendEmailGive($order);
        return redirect()->route('orders-orders')->with(
            'success',
            'Книги выданы клиенту'
        );
    }

    /**
     * @param  Order  $order
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * При нажатии библиотекарем на кнопку
     * "Ордер получен", то есть когда чел
     * вернул свой Ордер с книжками.
     */
    public function acceptOrder(Order $order)
    {
        Order::deleteOrder($order);
        return redirect()->back();
    }
}
