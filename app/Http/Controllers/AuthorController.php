<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorRequest;
use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * Class AuthorController
 *
 * @package App\Http\Controllers
 */
class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * REST По авторам
     */
    public function index()
    {
        $authors = Author::with('publisher')->paginate(3);
        return view('auth.authors.index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.authors.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AuthorRequest $request)
    {
        $data = $request->all();
        unset($data['image']);
        if ($request->has('image')) {
            $data['image'] = $request->file('image')->store('authors');
        }
        $author = Author::create($data);
        return redirect()->route('authors.index')->with(
            'success',
            'автор добавлена'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Author  $author
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        return view('auth.authors.show', compact('author'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Author  $author
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        return view('auth.authors.form', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Author  $author
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AuthorRequest $request, Author $author)
    {
        $params = $request->all();
        unset($params['image']);
        if ($request->has('image')) {
            Storage::delete($author->image);
            $params['image'] = $request->file('image')->store('authors');
        }
        $author->update($params);
        return redirect()->route('authors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Author  $author
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author)
    {
        $author->delete();
        return redirect()->route('authors.index')->with(
            'success',
            'Автор удалён'
        );
    }
}
