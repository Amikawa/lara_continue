<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'        => 'required|min:3|max:255|',
            'description' => 'max:255|nullable',
            'code'        => 'required|min:3|max:255|unique:books,code',
            'count'       => '|numeric|min:0|max:255|nullable',

        ];
        if ($this->route()->named('books.update')) {
            $rules['code'] .= ','.$this->route()->parameter('book')->id;
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'required' => 'Поле :attribute обязательно для ввода',
            'min'      => 'Поле :attribute должно иметь минимум :min символов',
            'code.min' => 'Поле код должно содержать не менее :min символов',
            'max'      => 'Поле :attribute должно иметь минимум :min символов',

        ];
    }
}
