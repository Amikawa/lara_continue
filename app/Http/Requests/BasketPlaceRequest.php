<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BasketPlaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|min:3|max:30',
            'phone' => 'regex:/^[+[:digit:]][-[:digit:]]{4,20}$/'
        ];
        // 'regex:/^(+\|d)?(d\|\+){4,20}d?/',
    }

    public function messages()
    {
        return [
            'required'    => 'Поле :attribute обязательно для ввода',
            'min'         => 'Поле :attribute должно иметь минимум :min символов',
            'phone.regex' => 'Телефон: допустимые символы: цифры, +, -.',
            'max'         => 'Поле :attribute должно иметь минимум :min символов',

        ];
    }
}
