<?php


namespace App\traits;


use App\Models\Permission;
use App\Models\Role;

trait HasRolesAndPermissions
{
    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * @param  mixed  ...$roles
     *
     * @return bool
     */
    public function hasRole($role)
    {
        if ($this->roles->contains('slug', $role)) {
            return true;
        }

        return false;
    }


    /**
     * @param $permission
     *
     * @return bool
     */
    public function hasPermission($permission)
    {
        if ($this->permissions->contains('slug', $permission)) {
            return true;
        }

        return false;
    }


    /**
     * @param $permission
     *
     * @return bool
     */
    public function hasPermissionThroughRole($permission)
    {
        foreach ($permission->roles as $role) {
            if ($this->roles->contains($role)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $permission
     *
     * @return bool
     */
    protected function hasPermissionTo($permission)
    {
        return $this->hasPermissionThroughRole($permission)
            || $this->hasPermission($permission);
    }

    /**
     * @param  array  $permissions
     *
     * @return mixed
     */
    protected function getAllPermissions(array $permissions)
    {
        return Permission::whereIn('slug', $permissions)->get();
    }


    /**
     * @param  mixed  ...$permissions
     *
     * @return $this
     */
    public function givePermissionsTo(...$permissions)
    {
        $permissions = $this->getAllPermissions($permissions);
        if ($permissions === null) {
            return $this;
        }
        $this->permissions()->saveMany($permissions);
        return $this;
    }

    /**
     * @param  mixed  ...$permissions
     *
     * @return $this
     */
    public function deletePermissions(...$permissions)
    {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);
        return $this;
    }


    /**
     * @param  mixed  ...$permissions
     *
     * @return HasRolesAndPermissions
     */
    public function refreshPermissions(...$permissions)
    {
        $this->permissions()->detach();
        return $this->givePermissionsTo($permissions);
    }
}
