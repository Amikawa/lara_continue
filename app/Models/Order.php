<?php

namespace App\Models;

use App\Mail\GiveOrder;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Order extends Model
{
    protected $guarded = [];

    protected $dates = [
        'expired_at',
    ];

    public function books()
    {
        return $this->belongsToMany(Book::class)->withPivot('count')->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function saveOrder($name, $phone)
    {
        if ($this->status == 0) {
            $this->name = $name;
            $this->phone = $phone;
            $this->status = 1;
            $this->save();
            session()->forget('orderId');
            return true;
        } else {
            return false;
        }
    }

    public function totalBooks()
    {
        $sum = 0;
        foreach($this->books()->withTrashed()->get() as $book)
        {
            $sum += $book->pivot->count;

        }
        return $sum;
    }

    public static function deleteOrder(Order $order)
    {
        foreach($order->books()->withTrashed()->get() as $book)
        {
            $count = $book->pivot->count;
            $book->reserved -= $count;
            $book->update();
        }
        self::destroy($order->id);
        session()->flash('success', "Заказ удалён с базы данных");

    }
    public function getStatusNameAttribute()
    {
        return $this->status == 1 ? "Забронировано" : "Выдано";
    }

    public function scopeReserved($query)
    {
        return $query->where('status', 1);
    }

    public function scopeGotten($query)
    {
        return $query->where('status', 2);
    }

    public function scopeByUserId($query, $id)
    {
        return $query->where('user_id', $id);
    }



    public static function sendEmailGive(Order $order)
    {

        Mail::to($order->user->email)->send(new GiveOrder($order));
        $order->status = 2;
        //когда стоит вернуть книгу в библиотеку
        $order->expired_at = now()->addMonth();
        $order->update();

        // нужно каждый день сравнивать expired_at и now->isCurrentDay()

    }


}
