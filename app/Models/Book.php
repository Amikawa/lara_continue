<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravelista\Comments\Commentable;
class Book extends Model
{
    use Commentable, SoftDeletes;
    protected $fillable
        = [
            'name', 'code', 'category_id', 'description', 'image', 'count', 'total', 'reserved'
        ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeByCode($query, $code)
    {
        return $query->where('code', $code);
    }

    public function isAvailable()
    {
        return $this->total > 0 && !$this->trashed();
    }

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

}
