<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

    public $timestamps = false;

    protected $fillable = ['first_name','second_name', 'publisher_id', 'image'];

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    public function books()
    {
        return $this->hasMany(Book::class);
    }
}
