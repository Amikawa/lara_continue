<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'city'];

    public function authors()
    {
        return $this->hasMany(Author::class);
    }

    public function books()
    {
        return $this->hasManyThrough(Book::class, Author::class);
    }

}
