### Task
#### Разработать веб-сервис "Библиотека".

3 роли: администратор, библиотекарь, клиент

Администратор:  
Может добавлять и удалять пользователей, устанавливать пароли.

Библиотекарь:  
Может добавлять и удалять книги  
Может выдавать и принимать книги от клиентов

Клиент:  
Может просматривать имеющиеся книги  
Искать по автору, жанру, издателю  
Бронировать книги, снимать бронь

Бизнес-процесс:  
Клиент заходит на сервис, бронирует книгу, затем приходит в библиотеку.  
Далее библиотекарь выдает книгу клиенту.  
Через некоторое время библиотекарь принимает книгу от клиента.  
Забронированную и выданную книгу нельзя забронировать и выдать  
Время бронирования ограничено.

Приветствуются дополнительные функции, например:  
Отправка пароля клиенту на электронный ящик, либо изменение и восстановление пароля клиентом.  
Уведомление клиента о том, что книга снова доступна для бронирования (по почте либо на сервисе).  
Возможность оценивать книги и писать комментарии о книге, чтобы другие пользователи могли видеть их.

Стек технологий:  
Vue JS  
Laravel 7.*  
Mysql / PG на выбор  

Верстка под хром.

Залить всё на bitbucket, скинуть ссылку на репозиторий.





### Resolving this task.

```sh
cd <directory_for_app>
```

- open project page on  [BitBucket](https://bitbucket.org/Amikawa/lara_continue/src/master/) and type on terminal: 

```sh  
git clone https://Amikawa@bitbucket.org/Amikawa/lara_continue.git 
```

- run command for loading all project dependencies:

```sh
composer install
npm i 
npm run dev
```

After downloading project, open him on any text redactor(Ide) and configure **.env.example** file:  
1. configure mysql connection on **DB** section  
2. run `php artisan key:generate`
3. change option FILESYSTEM_DRIVER to _public_  
4. change MAIL_DRIVER=log if u have not configured mail services.  
5. APP_URL option equate to appropriate web-server listening for url. In our example it _http://localhost:8181_.  
6. rename .env.example to .env  
7. run `php artisan migrate --seed`
If you don't links web-server to your project, run:

```sh   
php artisan serve --port=8181
php artisan storage:link
```

now open your web-browser and move to _http://localhost:8181 or your server listening url  

**End**


