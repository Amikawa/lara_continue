<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'books',
            function (Blueprint $table) {
                $table->softDeletes();
                $table->integer('author_id')->nullable();
                $table->foreign('author_id')->references('id')->on('authors')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'books',
            function (Blueprint $table) {
                $table->dropSoftDeletes();
                $table->dropColumn('author_id');
            }
        );
    }
}
