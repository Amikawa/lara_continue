<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'authors',
            function (Blueprint $table) {
                $table->id()->primary();
                $table->string('first_name');
                $table->string('second_name');
                $table->integer('publisher_id')->nullable();
                $table->string('image')->nullable();
                $table->foreign('publisher_id')->references('id')->on('publishers')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
