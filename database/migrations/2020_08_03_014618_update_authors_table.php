<?phpожности приложения
Код / применяемость (знания) фреймворка 5/10
https://bitbucket.org/Amikawa/lara_continue/src/master/app/Http/Controllers/BasketController.php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'authors',
            function (Blueprint $table) {
                $table->string('full_name')->storedAs(
                    'CONCAT(first_name, " ", second_name)'
                );
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'authors',
            function (Blueprint $table) {
                $table->dropColumn('full_name');
            }
        );
    }
}
