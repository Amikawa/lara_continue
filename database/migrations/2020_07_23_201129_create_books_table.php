<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'books',
            function (Blueprint $table) {
                $table->bigIncrements('id')->primary();
                $table->timestamps();
                $table->string('name');
                $table->integer('category_id');
                $table->text('description')->nullable();
                $table->string('code');
                $table->text('image')->nullable();
                $table->integer('count')->default(10);
                $table->foreign('category_id')->references('id')->on('categories')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
