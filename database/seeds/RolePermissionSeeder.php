<?php

use App\Models\Permission;
use App\Models\Role;

use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::find(1);
        $labRole = Role::find(2);

        $addDeleTePermission = Permission::find(1);
        $addDeleTeBooksPermission = Permission::find(2);

        $adminRole->permissions()->attach($addDeleTePermission);
//         $adminRole->permissions()->attach($addDeleTeBooksPermission);
        $labRole->permissions()->attach($addDeleTeBooksPermission);
    }
}
