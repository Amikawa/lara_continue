<?php

use Illuminate\Database\Seeder;

class PublisherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('publishers')->insert(
            [
                [
                    'name' => 'Издательство "Сомко"',
                    'city' => 'Санкт-Петербург',
                ],
                [
                    'name' => 'Издательство "Космо',
                    'city' => 'Казань',
                ],
                [
                    'name' => 'Издательство "Моско',
                    'city' => 'Москва',
                ],
            ]
        );
    }
}
