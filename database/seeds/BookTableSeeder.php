<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
                    'name'        => 'Гарри Поттер',
                    'code'        => 'potter',
                    'description' => 'Отличный фентези про мальчика, учащегося в волшебной школе',
                    'reserved'    => rand(0, 10),
                    'category_id' => 1,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Война и мир',
                    'code'        => 'voina_i_mir',
                    'description' => 'Шедевр Льва Толстого в четыре тома',
                    'reserved'    => rand(0, 10),
                    'category_id' => 2,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Наруто.Ураганные хроники.',
                    'code'        => 'naruto',
                    'description' => 'Легендарная манга про мальчика с демоном внутри',
                    'reserved'    => rand(0, 10),
                    'category_id' => 3,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Тетрадь смерти',
                    'code'        => 'death_note',
                    'description' => 'Хороший детектив, где главный герой получает тетрадь смерти..',
                    'reserved'    => rand(0, 10),
                    'category_id' => 3,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Хроники Дарьи Донцовой',
                    'code'        => 'darya',
                    'description' => 'Детeктив Дарьи Донцовой..',
                    'reserved'    => rand(0, 10),
                    'category_id' => 1,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Капитанская дочь',
                    'code'        => 'capitan',
                    'description' => 'Одно из произведений А.С. Пушкина.',
                    'reserved'    => rand(0, 10),
                    'category_id' => 3,
                    'image'       => '',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Капитанская дочь',
                    'code'        => 'Loremds',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore !',
                    'reserved'    => rand(0, 10),
                    'category_id' => 1,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Капитанская дочь',
                    'code'        => 'Loremd',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore!',
                    'reserved'    => rand(0, 10),
                    'category_id' => 2,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Капитанская дочь',
                    'code'        => 'Lorem',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
                    'reserved'    => rand(0, 10),
                    'category_id' => 3,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Капитанская дочь',
                    'code'        => 'Loremdsf',
                    'description' => 'Для самых смелых идей',
                    'reserved'    => rand(0, 10),
                    'category_id' => 2,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
                [
                    'name'        => 'Капитанская дочь',
                    'code'        => 'Loremhbgfg',
                    'description' => 'Любите домашние котлеты? Вам определенно стоит посмотреть на эту мясорубку!',
                    'reserved'    => rand(0, 10),
                    'category_id' => 1,
                    'image'       => 'd',
                    'author_id'   => rand(1, 3),
                ],
            ]
        );
    }
}
