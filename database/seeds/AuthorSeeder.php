<?php

use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert(
            [
                [
                    'first_name'   => 'Александр',
                    'second_name'  => 'Пушкин',
                    'publisher_id' => 1,
                    'image'        => '',
                ],
                [
                    'first_name'   => 'Вася',
                    'second_name'  => 'Пупкин',
                    'publisher_id' => 2,
                    'image'        => '',
                ],
                [
                    'first_name'   => 'Виталий',
                    'second_name'  => 'Наливкин',
                    'publisher_id' => 3,
                    'image'        => '',
                ],
            ]
        );
    }

}
