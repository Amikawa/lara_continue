<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                [
                    'name'        => 'Фантастика',
                    'code'        => 'fantasy',
                    'description' => 'В этом разделе вы найдёте самые популярные книги в жанре фантастика!',
                    'image'       => 'https://www.mirf.ru/wp-content/uploads/2020/04/Zahod.jpeg',
                ],
                [
                    'name'        => 'Классическая литература',
                    'code'        => 'classic',
                    'description' => 'Раздел с Классической литературой.',
                    'image'       => 'https://nstar-spb.ru/upload/iblock/b88/b888e2a6f3d0f83c7069f146df2ae5ba.jpg',
                ],
                [
                    'name'        => 'Манга',
                    'code'        => 'manga',
                    'description' => 'Раздел с мангой',
                    'image'       => 'https://you-anime.ru/manga-images/posters/pets-aesthetics-manga.jpg',
                ],
            ]
        );
    }
}
